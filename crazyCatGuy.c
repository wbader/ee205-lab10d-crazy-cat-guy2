///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01d - Crazy Cat Guy - EE 205 - Spr 2022
///
/// @file    crazyCatGuy.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o crazyCatGuy crazyCatGuy.c
///
/// Usage:  crazyCatGuy n
///   n:  Sum the digits from 1 to n
///
/// Result:
///   The sum of the digits from 1 to n is XX
///
/// Example:
///   $ ./crazyCatGuy 6
///   The sum of the digits from 1 to 6 is 21
///
///
///
/// @author  Waylon Bader <wbaderhawaii.edu>
/// @date    20 Jan 2021
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>

int main( int argc, char* argv[] ) {
   int n = atoi( argv[1] );
   int total = 0;
   for(int i = 1; i <= n; i=i+1)
      total += i;

   printf("The sum of the digits from %d to %d is %d\n", 1, n, total);

   return 0;
}
